﻿using UnityEngine;
using System.Collections;

public class Floor : ScrollingBase {
    public override void DestroyMe()
    {
        FloorManager.SpawnFloor(gameObject.transform.position.z - FloorManager.EndZ);
        Destroy(gameObject);
    }
}
