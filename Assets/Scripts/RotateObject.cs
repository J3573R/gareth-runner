﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

    public Vector3 Speed;
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.Rotate(Speed);
	}
}
