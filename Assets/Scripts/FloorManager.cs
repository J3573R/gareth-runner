﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloorManager : MonoBehaviour {

    public List<GameObject> piecePrefabs;
    public List<GameObject> enemyPrefabs;
    public List<GameObject> collectablePrefabs;
    public Vector3 Speed = Vector3.zero;
    public float StartZ;
    public float EndZ;

    public void Start()
    {
        float zPosition = 0;
        float floorLenght = Mathf.Abs(StartZ) + Mathf.Abs(EndZ);

        while (zPosition > -(floorLenght) - 15) {
            SpawnFloor(zPosition, false);
            zPosition -= 5;
        }
    }

    public void SpawnFloor(float offset, bool random = true)
    {
        if (random)
        {
            int index = Random.Range(0, piecePrefabs.Count);
            Instantiate(piecePrefabs[index], new Vector3(0, 0, StartZ + offset), Quaternion.identity);

            int spawnEnemy = Random.Range(0, 4);

            if (spawnEnemy == 0)
            {
                SpawnEnemy(0);
            }

            int spawnCollectable = Random.Range(0, 5);
            if (spawnCollectable == 0)
            {
                SpawnCollectable(0);
            }

        }
        else
        {
            Instantiate(piecePrefabs[0], new Vector3(0, 0, StartZ + offset), Quaternion.identity);
        }        
    }

    public void SpawnEnemy(float offset)
    {
        int index = Random.Range(0, enemyPrefabs.Count);
        int randomXPosition = Random.Range(-2, 2);
        int randomZPosition = Random.Range((int)StartZ, (int)StartZ + 4);
        //Instantiate(enemyPrefabs[index], new Vector3(randomXPosition, 1, StartZ + offset), Quaternion.identity);
        Instantiate(enemyPrefabs[index], new Vector3(randomXPosition, 1, randomZPosition + offset), Quaternion.identity);
    }

    public void SpawnCollectable(float offset)
    {
        int randomXPosition = 0;
        int randomZPosition = 0;

        while (true)
        {

            randomXPosition = Random.Range(-2, 2);
            randomZPosition = Random.Range((int)StartZ, (int)StartZ + 4);

            if (Physics.Raycast(new Vector3(randomXPosition, 2f, randomZPosition + offset), Vector3.down, 2))
            {
                Debug.DrawLine(new Vector3(randomXPosition, 2f, randomZPosition + offset), new Vector3(randomXPosition, 0f, randomZPosition + offset), Color.red, 2);
                break;
            }
        }


        //randomXPosition = Random.Range(-2, 2);
        int index = Random.Range(0, enemyPrefabs.Count);
        Instantiate(collectablePrefabs[index], new Vector3(randomXPosition, 1, randomZPosition + offset), Quaternion.identity);
    }
}