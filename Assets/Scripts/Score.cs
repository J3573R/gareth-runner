﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public Text _highScore;
    public Text _score;
    public Text _coins;
    public GameObject _gameOver;
	
	void Update () {
        _highScore.text = "Highscore: " + Globals.HighScore.ToString();
        _score.text = "Score: " + Globals.Score.ToString();
        _coins.text = "Coins: " + Globals.Coins.ToString();

        if (Globals.IsAlive)
        {
            _gameOver.SetActive(false);
        } else
        {
            _gameOver.SetActive(true);
        }
	}
}
