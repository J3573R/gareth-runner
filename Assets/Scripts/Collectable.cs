﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : ScrollingBase {

    void Awake()
    {
        base.Awake();
        gameObject.transform.rotation = Quaternion.Euler(90, 0, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Globals.Coins++;
            Destroy(gameObject);
        }
    }

}
