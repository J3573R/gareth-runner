﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals {

    public static bool IsAlive;
    public static int Score;
    public static int Coins;
    public static int HighScore;
}
