﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_Game : GameState {

    private GameStateManager _gameStateManager;
    private FloorManager _floorManager;
    private float _lastScore;

    public override void Activate()
    {
        _floorManager = GameObject.Find("FloorManager").GetComponent<FloorManager>();
        _gameStateManager = GameObject.Find("Game State Manager").GetComponent<GameStateManager>();
        _lastScore = 0;
        Globals.Score = 0;
        Globals.IsAlive = true;
    }

    public State_Game()
    {
        SceneName = "level1";
    }

    public override void Update()
    {
        if (!Globals.IsAlive)
        {
            if (_floorManager != null)
            {
                _floorManager.Speed = Vector3.zero;
            }

            if (Globals.Score > Globals.HighScore)
            {
                Globals.HighScore = Globals.Score;
            }
            
            if (Input.anyKey)
            {
                _gameStateManager.ChangeState(GameStateManager.GameStates.Game);
            }
        }
        else
        {
            _lastScore += Time.deltaTime;
            if(_lastScore >= 1)
            {
                Globals.Score++;
                _lastScore = 0;
            }
        }

        //Debug.Log("Score:" + Globals.Score);
    }
}
