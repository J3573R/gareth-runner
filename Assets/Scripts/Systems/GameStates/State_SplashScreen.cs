﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_SplashScreen : GameState
{

    private float _timeToChange = 3;

    public State_SplashScreen()
    {
        SceneName = "SplashScreen";
    }

    public override void Update()
    {
        if (_timeToChange <= 0)
        {
            GameStateManager.Instance.ChangeState(GameStateManager.GameStates.MainMenu);
        }
        _timeToChange -= Time.deltaTime;
    }
}
