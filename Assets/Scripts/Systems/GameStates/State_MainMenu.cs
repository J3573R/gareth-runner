﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class State_MainMenu : GameState
{
    private Button _playButton;
    private Button _exitButton;

    public State_MainMenu()
    {
        SceneName = "MainMenu";
    }

    public override void Activate()
    {
        base.Activate();
        _playButton = GameObject.Find("PlayButton").GetComponent<Button>();
        _exitButton = GameObject.Find("ExitButton").GetComponent<Button>();
        _playButton.onClick.AddListener(PressPlay);
        _exitButton.onClick.AddListener(PressExit);
    }

    public override void Update()
    {
        //Debug.Log("Running Main menu");
    }

    public void PressPlay()
    {
        GameStateManager.Instance.ChangeState(GameStateManager.GameStates.Game);
    }

    public void PressExit()
    {
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;
    }
}
