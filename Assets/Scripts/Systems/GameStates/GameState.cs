﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState
{
    private string _sceneName = "BaseClass";
    
    public string SceneName
    {
        get { return _sceneName; }
        protected set { _sceneName = value; }
    }

    public virtual void Activate()
    {
        Debug.Log("State enabled: " + _sceneName);
    }

    public virtual void Update()
    {
        Debug.Log("Running BaseClass");
    }

    public virtual void Disable()
    {
        Debug.Log("State disabled: " + _sceneName);
    }
}
