﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour
{
    #region Singleton
    public static GameStateManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        Instance = this;
        DontDestroyOnLoad(Instance);
        if (_defaultState != GameStates.None)
        {
            SetState(_defaultState);
        }
        GameState.Activate();
    }
    #endregion

    public enum GameStates
    {
        None,
        SplashScreen,
        MainMenu,
        Game
    }

    [SerializeField] private GameStates _defaultState; 
    public GameState GameState;

    
    public void ChangeState(GameStates state)
    {
        SetState(state);
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.LoadScene(GameState.SceneName);
    }

    public void Update()
    {
        GameState.Update();
    }

    private void SetState(GameStates state)
    {
        if (GameState != null)
        {
            GameState.Disable();
        }

        switch (state)
        {
            case GameStates.SplashScreen:
                GameState = new State_SplashScreen();
                break;
            case GameStates.MainMenu:
                GameState = new State_MainMenu();
                break;
            case GameStates.Game:
                GameState = new State_Game();
                break;
        }
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode loadSceneMode)
    {
        GameState.Activate();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

}
