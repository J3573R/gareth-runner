﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

    public float Speed;
    public float Gravity;

    private Vector3 _left = new Vector3(-1, 0, 0);
    private Vector3 _right = new Vector3(1, 0, 0);

    private Vector3 _currentPosition;
    private Vector3 _futurePosition;
    private float _time = 1;
    private float _verticalVelocity = 0;

    private int _minMovement = -2;
    private int _maxMovement = 2;

    FloorManager FloorManager;

    private enum PlayerState
    {
        Running,
        Jumping,
        Falling
    }
    
    PlayerState _state = PlayerState.Running;

    void Awake()
    {
        FloorManager = FindObjectOfType<FloorManager>();
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.freezeRotation = true;
    }


    // Update is called once per frame
    void Update () {

        if (_time < 1)
        {
            _time += Time.deltaTime * Speed;
            transform.position = Vector3.Lerp(_currentPosition, _futurePosition, _time);
        }
        else
        {
            if (PressJump())
            {
                if (gameObject.transform.position.y == 1)
                {
                    _state = PlayerState.Jumping;
                    _verticalVelocity = 5;
                }
            }

            if (PressLeft() && OnGround() && gameObject.transform.position.x != _minMovement)
            {
                _currentPosition = gameObject.transform.position;
                _futurePosition = gameObject.transform.position + _left;
                _time = 0;
            }

            if (PressRight() && OnGround() && gameObject.transform.position.x != _maxMovement)
            {
                _currentPosition = gameObject.transform.position;
                _futurePosition = gameObject.transform.position + _right;
                _time = 0;
            }
        }
    }

    void FixedUpdate()
    {
        CheckFloor();

        gameObject.transform.position = new Vector3(
            gameObject.transform.position.x,
            gameObject.transform.position.y + _verticalVelocity * Time.deltaTime,
            gameObject.transform.position.z);


        if (gameObject.transform.position.y <= 1 && _state != PlayerState.Falling)
        {
            _verticalVelocity = 0;
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 1, gameObject.transform.position.z);
            _state = PlayerState.Running;
        }
        else
        {
            _verticalVelocity -= Gravity;
        }
        
    }

    private bool PressLeft()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            return true;
        }
        return false;
    }

    private bool PressRight()
    {
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            return true;
        }
        return false;
    }

    private bool PressJump()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            return true;
        }
        return false;
    }

    private bool OnGround()
    {
        if (gameObject.transform.position.y == 1)
        {
            return true;
        }
        return false;
    }

    private void CheckFloor()
    {
        Vector3 down = transform.TransformDirection(Vector3.down);
        down = down.normalized;
        if (!Physics.Raycast(gameObject.transform.position, down, 1))
        {
            if (_state == PlayerState.Running)
            {                
                if (FloorManager != null)
                {
                    FloorManager.Speed = Vector3.zero;
                    _state = PlayerState.Falling;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Floor")
        {
            Globals.IsAlive = false;
        }
    }
}
