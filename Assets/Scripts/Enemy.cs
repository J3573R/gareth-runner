﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : ScrollingBase
{

    public GameObject Explosion;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Instantiate(Explosion, transform.position, Quaternion.identity);
            Globals.IsAlive = false;
            Destroy(gameObject);
        }
    }
}
