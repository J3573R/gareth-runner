﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBase : MonoBehaviour {

    protected FloorManager FloorManager;
    private Vector3 _vPos;

	protected void Awake()
    {
        FloorManager = FindObjectOfType<FloorManager>();
    }
	
	protected void Update () {
	    _vPos = gameObject.transform.position + FloorManager.Speed * Time.deltaTime;
	    gameObject.transform.position = _vPos;
        if (gameObject.transform.position.z <= FloorManager.EndZ)
        {
            DestroyMe();
        }
        
	}

    public virtual void DestroyMe()
    {
        Destroy(gameObject);
    }
}
